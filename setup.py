"""Setup module for editable installs."""

import sys

import setuptools
from setuptools import setup

setuptools_min = 68
setuptools_major = int(setuptools.__version__.split('.', 1)[0])
info_args = {'egg_info', 'dist_info'}
if setuptools_major < setuptools_min and not info_args.intersection(sys.argv):
    message = (
        f'setuptools-{setuptools.__version__} from {setuptools.__path__}'
        ' is in use, but setuptools>=68.0 is required'
        )
    raise NotImplementedError(message)

setup(setup_requires=[
    'setuptools>=68.0',
    'setuptools_scm[toml]>=8',
    'wheel',
    ])
