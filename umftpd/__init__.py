"""umftpd - File Transfer Server."""

import collections
import collections.abc
import concurrent.futures
import contextlib
import datetime
import functools
import itertools
import logging
import os
import pathlib
import re
import time
import typing

import umftpd.aio as aio
import umftpd.config as cfg
import umftpd.crypto as crypto
import umftpd.ipc as ipc
import umftpd.server as srv
import umftpd.ui as ui
import umftpd.uitk as uitk

from gi.repository import Adw, Gdk, Gio, GLib, Gtk

__author__ = 'Felipe A Hernandez <ergoithz@gmail.com>'

logger = logging.getLogger(__name__)

T = typing.TypeVar('T')

RE_MARKUP = re.compile('<[^>]+>')


class Application(uitk.UiApplication):

    application_id = 'eu.ithz.umftpd'

    window: Adw.ApplicationWindow | None = None
    window_size_request: tuple[int, int] | None = None
    allow_localhost: bool = False

    stopping: bool = False
    scrolled: bool = True

    invalid: set
    hosts: tuple[str, ...] = ()

    max_log_lines = 4096

    server: concurrent.futures.Future = aio.resolved()
    server_timeout: float = 0.5
    server_error_flush: float = 0
    server_statuses = 'starting', 'online', 'offline'

    status_time: float = 0

    aioprocess: aio.AIOProcess | None = None
    actions = *aio.actions, *srv.actions
    action_accels: typing.ClassVar = {
        'win.help': ['F1'],
        }

    main_options: typing.ClassVar[dict] = {
        'window-size': (
            b's',
            0,
            GLib.OptionArg.STRING,
            'Desired window size',
            'WIDTHxHEIGHT',
            ),
        'adaptative': (
            0,
            0,
            GLib.OptionArg.NONE,
            'Follow desktop dark/light mode setting',
            ),
        'verbose': (
            0,
            0,
            GLib.OptionArg.NONE,
            'Log debug information',
            ),
        'localhost': (
            0,
            0,
            GLib.OptionArg.NONE,
            'Allow localhost and disable network check',
            ),
        }

    protocol_reverse_cycling: bool = False
    protocols = 'ftp', 'ftps', 'sftp'

    logger_name = f'{__name__}:application'
    logger_names = logger_name, *aio.logger_names

    exit_status: int = 0

    @property
    def initialized(self) -> bool:
        return self.aioprocess is not None

    @property
    def server_addresses(self) -> tuple[str, ...]:
        username = self.config.auth.username
        port = self.config.port
        return tuple(
            f'{protocol}://{username}@{host}:{port}'
            for protocol in self.config.protocols
            for host in (f'[{h}]' if ':' in h else h for h in self.hosts)
            )

    @property
    def server_type(self) -> str:
        protocol = self.protocols[self.config.secure]
        return uitk.ref[f'protocol_switch_{protocol}'].get_label()

    @property
    def home(self) -> str:
        path = self.config.directory
        return (
            cfg.USER_HOME.as_posix()
            if path == cfg.USER_HOME else
            f'~/{path.relative_to(cfg.USER_HOME).as_posix()}'
            if path.is_relative_to(cfg.USER_HOME) else
            path.absolute().as_posix()
            )

    @functools.cached_property
    def protocol_tooltips(self) -> tuple[str, ...]:
        return tuple(
            ui.strings.render(f'protocol_{proto}_tooltip')
            for proto in self.protocols
            )

    @functools.cached_property
    def config(self) -> 'cfg.Config':
        return cfg.Config.from_environment()

    @functools.cached_property
    def user_directories(self) -> tuple[tuple[pathlib.Path, str, str], ...]:
        theme = Gtk.IconTheme.get_for_display(uitk.ref.main.get_display())
        home = pathlib.Path.home()
        root = pathlib.Path(home.anchor)
        special_dirs = (
            (pathlib.Path(path), key[10:].replace('_', '').lower())
            for key, path in (
                (key, GLib.get_user_special_dir(value))
                for key, value in vars(GLib.UserDirectory).items()
                if key.startswith('DIRECTORY_')
                )
            if path
            )
        return (
            (home, ui.strings.directory_home, 'user-home'),
            (root, ui.strings.directory_computer, 'drive-harddisk'),
            *(
                (path, path.name, next(
                    filter(theme.has_icon, (f'folder-{slug}', f'user-{slug}')),
                    'folder',
                    ))
                for path, slug in special_dirs
                ),
            )

    @uitk.app_connect('handle-local-options')
    def on_local_options(
            self,
            app: 'Application',
            options: GLib.VariantDict,
            ) -> typing.Literal[-1]:
        if size := options.lookup_value('window-size', GLib.VariantType('s')):
            w, h = map(int, size.get_string().split('x'))
            self.window_size_request = w, h
        if not options.lookup_value('adaptative', GLib.VariantType('b')):
            style_manager = self.get_style_manager()
            style_manager.set_color_scheme(Adw.ColorScheme.PREFER_DARK)
        self.allow_localhost = bool(
            options.lookup_value('localhost', GLib.VariantType('b')),
            )
        return -1

    @uitk.app_connect('startup')
    def on_startup(self, app: 'Application') -> None:
        self.aioprocess = aio.AIOProcess(timeout=self.server_timeout)
        self.aioprocess.add_done_callback(self.aioprocess_callback)
        self.aioprocess.start()

    @uitk.app_connect('activate')
    def on_activate(self, app: 'Application') -> None:
        if self.window_size_request:
            uitk.ref.main.set_default_size(*self.window_size_request)

        # FIXME: research why focusing during declaration breaks autosizing
        uitk.ref.server_start.grab_focus()

        uitk.ref.main.present()

    @uitk.app_connect('shutdown')
    def on_shutdown(self, app: 'Application') -> None:
        self.stopping = True
        self.aioprocess.cancel()

    @uitk.app_action('server:session', args=('server_active_sessions', 1))
    @uitk.app_action('server:disconnect', args=('server_active_sessions', -1))
    @uitk.app_action('server:upload', args=('server_total_uploads', 1))
    @uitk.app_action('server:download', args=('server_total_downloads', 1))
    def on_server_stat(
            self,
            action: Gio.Action, data: GLib.Variant,
            target: str, change: int,
            ) -> None:
        self.server_stats[target] += change
        text = ui.strings.render(target, num=self.server_stats[target])
        uitk.ref[target].set_label(text)

    @uitk.app_action('server:start')
    def on_server_start(self, action: Gio.Action, data: GLib.Variant) -> None:
        message = ui.strings.render.server_log_info(
            config=self.config,
            home=self.home,
            addresses=self.server_addresses,
            )
        self.logger.info(
            RE_MARKUP.sub('', message),
            extra={'markup': message, 'scroll': True},
            )
        GLib.idle_add(self.set_status, 'online')
        GLib.idle_add(
            uitk.ref.status_page.set_icon_name,
            ui.STATUS_ICON_CONNECTED,
            )

    @uitk.connect('about_dialog', 'close-request')
    @uitk.connect('directory_dialog', 'response')
    @uitk.connect('directory_root_error', 'response')
    def on_dialog(self, widget: Gtk.Dialog, res: typing.Any = None) -> None:
        widget.hide()

    @uitk.connect('directory_dialog', 'response')
    def on_directory(
            self,
            widget: Gtk.FileChooserNative,
            res: Gtk.ResponseType,
            ) -> None:
        if res == Gtk.ResponseType.ACCEPT:
            selected = widget.get_file()
            if not selected:
                # WORKAROUND(flatpak bug): show error dialog
                # https://github.com/flatpak/xdg-desktop-portal/issues/820
                uitk.ref.directory_error.show()
                return
            path = selected.get_path()
            if cfg.FLATPAK and path == '/':
                # WORKAROUND(flatpak/fuse limitation): show error dialog
                # fuse cannot mount root, so flatpak returns its internal root
                uitk.ref.directory_root_error.show()
                return
            self.set_directory(pathlib.Path(path))

    @uitk.action('main', 'help')
    @uitk.action('main', 'known-issues')
    def on_uri_action(self, action: Gio.Action, data: None) -> None:
        name = action.get_name()
        Gtk.show_uri(uitk.ref.main, cfg.urls[name], Gdk.CURRENT_TIME)

    @uitk.action('main', 'about')
    def on_about(self, action: Gio.Action, data: None) -> None:
        uitk.ref.about_dialog.present()

    @uitk.connect('username_entry_delegate', 'state-flags-changed',
                  args=('username',))
    @uitk.connect('password_entry_delegate', 'state-flags-changed',
                  args=('password',))
    def on_entry_state_changed(
            self,
            widget: Gtk.Text,
            previous: int,
            name: str,
            ) -> None:
        reset: Gtk.Button = uitk.ref[f'{name}_entry_reset']
        focused = Gtk.StateFlags.FOCUS_WITHIN | Gtk.StateFlags.ACTIVE
        focus_lost = (
            previous & focused
            and not widget.get_state_flags() & focused
            and not reset.get_state_flags() & focused
            )
        if focus_lost:
            self.set_entry(name, widget.get_text() or None)

    @uitk.connect('username_entry_delegate', 'activate', args=('username',))
    @uitk.connect('password_entry_delegate', 'activate', args=('password',))
    def on_entry_activate(self, widget: Gtk.Text, name: str) -> None:
        self.set_entry(name, widget.get_text() or None)
        self.cycle_entry_focus(widget)

    @uitk.connect('username_entry_delegate', 'changed', args=('username',))
    @uitk.connect('password_entry_delegate', 'changed', args=('password',))
    def on_entry_change(self, widget: Gtk.Text, name: str) -> None:
        reset: Gtk.Button = uitk.ref[f'{name}_entry_reset']
        icons = 'edit-undo-symbolic', 'edit-clear-symbolic'
        tooltips = f'{name}_reset_button', f'{name}_clear_button'

        text = widget.get_text()
        text_is_default = text == getattr(self.config.default, name)

        reset.set_tooltip_text(ui.strings[tooltips[text_is_default]])
        reset.set_icon_name(icons[text_is_default])

        self.set_config(**{name: text})

    @uitk.connect('username_entry_reset', 'clicked', args=('username',))
    @uitk.connect('password_entry_reset', 'clicked', args=('password',))
    def on_entry_reset(self, widget: Gtk.Button, name: str) -> None:
        self.set_entry(name, None)

    @uitk.connect('directory_reset', 'clicked', args=('set_directory',))
    @uitk.connect('port_reset', 'clicked', args=('set_port',))
    def on_reset(self, widget: Gtk.Button, method: str) -> None:
        # set_directory; set_port
        getattr(self, method)(None)

    @uitk.connect('directory_button', 'clicked')
    def on_directory_click(self, widget: Gtk.Button) -> None:
        uitk.ref.directory_dialog.show()

    @uitk.connect('readonly_switch', 'state-set')
    def on_readonly_change(self, widget: Gtk.Switch, value: bool) -> None:
        readonly = widget.get_active()

        warning: Gtk.Revealer = uitk.ref.readonly_disabled
        warning.set_reveal_child(not readonly)
        self.set_config(readonly=readonly)

    @uitk.connect('port_entry', 'value-changed')
    def on_port_change(self, widget: Gtk.SpinButton) -> None:
        port = widget.get_value_as_int()
        uitk.ref.port_reset.set_sensitive(port != self.config.default.port)
        self.set_config(port=port)

    @uitk.connect('logs_toggle', 'toggled')
    def on_logs_toggle(self, widget: Gtk.ToggleButton) -> None:
        revealer: Gtk.Revealer = uitk.ref.logs_revealer
        toggle: Gtk.ToggleButton = uitk.ref.logs_toggle
        page: Adw.StatusPage = uitk.ref.status_page
        tooltips = ui.strings.server_logs_show, ui.strings.server_logs_hide

        active = widget.get_active()
        page.set_vexpand(not active)
        toggle.set_icon_name(ui.REVEAL_UP_ICONS[active])
        toggle.set_tooltip_text(tooltips[active])
        revealer.set_visible(active)
        revealer.set_reveal_child(active)
        self.set_config(ui={'logs': active})

    @uitk.connect('logs_revealer', 'show')
    def on_revealer_reveal(self, widget: Gtk.Revealer) -> None:
        self.scroll_logs_to_bottom()

    @uitk.connect('logs_vadjustment', 'value-changed')
    def on_logs_vadjustment(self, adjustment: Gtk.Adjustment) -> None:
        max_scroll = adjustment.get_upper() - adjustment.get_page_size()
        self.scrolled = adjustment.get_value() == max_scroll

    @uitk.connect('address_toggle', 'clicked')
    def on_address_toggle(self, widget: Gtk.ToggleButton) -> None:
        active = widget.get_active()
        widget.set_icon_name(ui.REVEAL_END_ICONS[active])
        uitk.ref.address_revealer.set_reveal_child(active)

    @uitk.connect('protocol_switch_ftp', 'toggled', args=(0,))
    @uitk.connect('protocol_switch_ftps', 'toggled', args=(1,))
    @uitk.connect('protocol_switch_sftp', 'toggled', args=(2,))
    def on_protocol_change(self, widget: Gtk.ToggleButton, value: int) -> None:
        if not widget.get_active():
            return

        protocols = self.protocols
        tooltips = self.protocol_tooltips

        if value in (0, len(protocols) - 1):
            self.protocol_reverse_cycling = bool(value)

        uitk.ref.protocol_wrapper.set_tooltip_markup(tooltips[value])

        for i, (proto, tooltip) in enumerate(
                zip(protocols, tooltips, strict=True),
                ):
            tip, visible = (None, True) if i == value else(tooltip, False)
            uitk.ref[f'protocol_switch_{proto}'].set_tooltip_markup(tip)
            uitk.ref[f'protocol_{proto}_label'].set_visible(visible)

        self.set_config(secure=value)

    @uitk.connect('protocol_row', 'activated')
    def on_protocol_click(self, widget: Gtk.Widget) -> None:
        inc = (1, -1)[self.protocol_reverse_cycling]
        self.set_protocol((self.config.secure + inc) % len(self.protocols))

    @uitk.connect('server_start', 'clicked', args=(True,))
    @uitk.connect('server_stop', 'clicked', args=(False,))
    def on_server_click(self, widget: Gtk.Widget, value: bool) -> None:
        self.set_online(value)

    @uitk.connect('share_clipboard', 'clicked')
    def on_clipboard_click(self, widget: Gtk.Button) -> None:
        clipboard = Gdk.Display.get_default().get_clipboard()
        clipboard.set(uitk.ref.address_label.get_label())
        toast = ui.UiAdwToast(ui.strings.share_address_notification)
        uitk.ref.main_toast.add_toast(toast)

    def cycle_entry_focus(self, current: Gtk.Widget) -> None:
        next_focus = next(
            item
            for item in self.focus_cycle[self.focus_cycle.index(current) + 1:]
            if item.is_sensitive()
            )
        next_focus.grab_focus()

    def set_entry(
            self,
            name: typing.Literal['username', 'password'],
            value: 'crypto.CryptoHash | str | None',
            ) -> None:
        entry: Adw.EntryRow | Adw.PasswordEntryRow = uitk.ref[f'{name}_entry']
        delegate: Gtk.Text = uitk.ref[f'{name}_entry_delegate']
        reveal: Gtk.Button | None = uitk.ref.get(f'{name}_entry_reveal')
        titles = f'{name}_title', f'{name}_encrypted_title'
        css_classes = [], ['dim-label']

        default = getattr(self.config.default, name)
        clear = entry.get_text() == default and not value

        is_encrypted = isinstance(value, crypto.CryptoHash)

        entry.set_text((
            '' if clear else
            delegate.get_invisible_char() * 16 if is_encrypted else
            value or default
            ))
        entry.set_title(ui.strings[titles[is_encrypted]])

        delegate.set_css_classes(css_classes[is_encrypted])
        delegate.set_editable(not is_encrypted)
        delegate.set_sensitive(not is_encrypted)

        if reveal:
            reveal.set_sensitive(not is_encrypted)

            if value is None:
                delegate.set_visibility(True)

            if is_encrypted:
                reveal.set_tooltip_text(ui.strings[f'{name}_encrypted_reveal'])

        if clear:
            entry.grab_focus()

    def set_port(self, port: int | None) -> None:
        entry: Gtk.SpinButton = uitk.ref.port_entry
        entry.set_value(port or self.config.default.port)

    def set_directory(self, path: os.PathLike | None) -> None:
        path = path or self.config.default.directory
        self.set_config(directory=path)

    def set_protocol(self, value: int) -> None:
        protocol = self.protocols[value]
        button: Gtk.ToggleButton = uitk.ref[f'protocol_switch_{protocol}']
        button.set_active(True)

    def set_readonly(self, readonly: bool) -> None:
        switch: Gtk.Switch = uitk.ref.readonly_switch
        switch.set_active(readonly)

    def set_logs_visible(self, state: bool) -> None:
        toggle: Gtk.ToggleButton = uitk.ref.logs_toggle
        toggle.set_active(state)

    def set_server_error(self, error: str | None = None) -> None:
        row = uitk.ref.server_start
        revealer = uitk.ref.server_error_revealer

        if error:
            self.server_error_flush = time.monotonic() + 1 + (
                revealer.get_transition_duration() / 1000.
                )
            uitk.ref.server_error_label.set_label(error)

        (row.add_css_class if error else row.remove_css_class)('error')
        revealer.set_reveal_child(error is not None)

    def set_online(self, state: bool) -> None:
        if state:
            if not self.validate_config():
                return

            self.set_status('starting')
            self.set_server_error()

        pages = uitk.ref.page_left, uitk.ref.page_right
        transitions = (
            Gtk.StackTransitionType.SLIDE_RIGHT,
            Gtk.StackTransitionType.SLIDE_LEFT,
            )

        uitk.ref.back_revealer.set_reveal_child(state)
        uitk.ref.stack.set_transition_type(transitions[state])
        uitk.ref.stack.set_visible_child(pages[state])

        if not state:
            self.server.cancel()
            return

        uitk.ref.server_type.set_label(ui.strings.render.server_status_type(
            type=self.server_type,
            ))

        for target in self.server_stats:
            uitk.ref[target].set_label(ui.strings.render(target, num=0))
        self.server_stats.clear()

        primary, *extra = self.server_addresses
        uitk.ref.address_label.set_label(primary)

        addrbox = uitk.ref.address_box
        uitk.ref.address_toggle.set_visible(bool(extra))
        uitk.ref.address_revealer.set_visible(bool(extra))
        for address, widget in itertools.zip_longest(extra, tuple(addrbox)):
            if address and widget:
                widget.set_label(address)
            elif address:
                addrbox.append(ui.UiAddressLabel(label=address))
            else:
                addrbox.remove(widget)

        uitk.ref.status_page.set_icon_name(ui.STATUS_ICON_DISCONNECTED)
        uitk.ref.address_toggle.grab_focus()

        self.server = self.aioprocess.submit(
            srv.Server.run,
            self.config,
            self.server_timeout,
            )
        self.server.add_done_callback(self.server_callback)

    def set_status(self, status: str, created: float | None = None) -> None:
        if created and created < self.status_time:
            return
        self.status_time = time.monotonic()

        for name in self.server_statuses:
            uitk.ref[f'server_status_{name}'].set_visible(name == status)

    def set_config(self, **kwargs) -> None:
        updates = {  # we need validation side-effects during initialization
            name: value
            for name, value in kwargs.items()
            if self.validate(name, value)
            }
        if self.initialized and updates:
            current = self.config
            updated = current.with_updates(**updates)
            if current != updated:
                updated = self.config = updated.with_updates(info={
                    'last': datetime.datetime.now(tz=cfg.TZ),
                    })
                updated.save()

    def validate_config(self) -> bool:
        self.hosts = srv.route_host(loopback=self.allow_localhost)

        if not self.hosts:
            self.set_server_error(ui.strings.server_error_no_host)
            self.logger.warning('Server start bounced, no route to host')
            return False

        if not self.server.done():
            self.set_server_error(ui.strings.server_error_running)
            self.logger.warning('Server start bounced, already running')
            return False

        for name, target in (
                ('port', 'port_entry'),
                ('directory', 'directory_button'),
                ):
            if not self.validate(name, getattr(self.config, name)):
                self.logger.warning('Server start bounced, invalid port')
                uitk.ref[target].grab_focus()
                return False

        return not self.invalid

    def validate_user(self, user: str) -> bool:
        return bool(user)

    def validate_password(self, password: str) -> bool:
        return bool(password)

    def validate_port(self, port: int) -> bool:
        box: Gtk.Box = uitk.ref.port_box
        revealer: Gtk.Revealer = uitk.ref.port_error_revealer

        valid = (
            srv.check_port(port, False)
            and srv.check_port(port, True)
            )

        (box.remove_css_class if valid else box.add_css_class)('error')
        revealer.set_reveal_child(not valid)
        return valid

    def validate_directory(self, directory: pathlib.Path) -> bool:
        revealer: Gtk.Revealer = uitk.ref.readonly_disabled
        warning: Gtk.Label = uitk.ref.readonly_disabled_label
        button: Adw.ButtonContent = uitk.ref.directory_content
        reset: Gtk.Button = uitk.ref.directory_reset
        switch: Gtk.Switch = uitk.ref.readonly_switch
        label, icon, readable, writable = self.identify_directory(directory)
        button.set_label(label)
        button.set_icon_name(icon)
        reset.set_sensitive(directory != self.config.default.directory)
        warning.set_css_classes([(
            'warning' if writable else
            'dim-label' if readable else
            'error'
            )])
        warning.set_label((
            ui.strings.readonly_disabled_warning if writable else
            ui.strings.readonly_directory_warning if readable else
            ui.strings.invalid_directory_warning
            ))
        revealer.set_reveal_child((
            not writable
            or not self.config.readonly
            ))
        switch.set_visible(writable)
        return readable

    def validate(self, name: str, value: typing.Any) -> bool:
        validator = getattr(self, f'validate_{name}', None)
        valid = validator is None or validator(value)
        (self.invalid.add, self.invalid.discard)[valid](name)
        return valid

    def identify_directory(self,
                           path: pathlib.Path,
                           ) -> tuple[str, str, bool, bool]:
        if path.is_dir() and not (cfg.FLATPAK and path.parent is path):
            writable = os.access(path, os.W_OK | os.R_OK | os.X_OK)
            readable = writable or os.access(path, os.R_OK | os.X_OK)
            suppress_not_found = contextlib.suppress(FileNotFoundError)
            for location, name, icon in self.user_directories:
                with suppress_not_found:
                    if location.samefile(path):
                        return name, icon, readable, writable
            return path.name, 'folder', readable, writable
        return path.name, 'action-unavailable-symbolic', False, False

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, application_id=self.application_id, **kwargs)

        self.invalid = set()
        self.server_stats = collections.defaultdict(int)

        for name, ar in self.main_options.items():
            self.add_main_option(name, *ar)

        uitk.ref.port_entry.set_range(srv.MIN_PORT, srv.MAX_PORT)

        self.set_entry('username', self.config.username)
        self.set_entry('password', (
            self.config.password
            or self.config.password_hash
            or None
            ))
        self.set_directory(self.config.directory)
        self.set_readonly(self.config.readonly)
        self.set_port(self.config.port)
        self.set_protocol(self.config.secure)
        self.set_logs_visible(self.config.ui.logs)

        aio.ActionLogHandler.setup(self.logger_names)
        self.logger = logging.getLogger(self.logger_names[0])

        self.focus_cycle = (
            uitk.ref.username_entry_delegate,
            uitk.ref.password_entry_delegate,
            uitk.ref.server_start,
            )

    @uitk.app_action('aio:log')
    def on_aio_log(self, action: Gio.Action, data: GLib.Variant) -> None:
        record = ipc.variant_loads(data)
        if not record:
            return

        created = datetime.datetime.fromtimestamp(record.created, tz=cfg.TZ)
        message = '<tt>{} {} </tt>{}\n'.format(
            created.replace(microsecond=0).isoformat(' '),
            record.levelname,
            (
                getattr(record, 'markup', None)
                or f'<b>{GLib.markup_escape_text(record.getMessage())}</b>'
                ),
            )

        textview: Gtk.TextView = uitk.ref.logs_textview
        textbuffer = textview.get_buffer()
        textbuffer.insert_markup(textbuffer.get_end_iter(), message, -1)

        lines = textbuffer.get_line_count()
        if line := max(0, lines - self.max_log_lines):
            found, cursor = textbuffer.get_iter_at_line(line)
            if found:
                textbuffer.delete(textbuffer.get_start_iter(), cursor)

        if self.scrolled or getattr(record, 'scroll', False):
            GLib.idle_add(self.scroll_logs_to_bottom)

    def scroll_logs_to_bottom(self) -> None:
        adjustment: Gtk.Adjustment = uitk.ref.logs_vadjustment
        adjustment.set_value(adjustment.get_upper())
        self.scrolled = True

    def quit_with_status(self, status: int) -> None:
        self.exit_status = status
        self.quit()

    def aioprocess_callback(self, future: concurrent.futures.Future) -> None:
        if not future.cancelled() and (exc := future.exception()):
            self.logger.error('Unrecoverable error', exc_info=exc)
            self.quit_with_status(1)

    def server_callback(self, future: concurrent.futures.Future) -> None:
        if not future.cancelled():
            try:
                future.result()
            except crypto.SSLCertificateError as exc:
                self.logger.exception('Certificate error: %s', *exc.args)
            except Exception:
                self.logger.exception('Server error')

        if not self.stopping:
            GLib.timeout_add(
                int(max(0, self.server_error_flush - time.monotonic()) * 1000),
                self.set_server_error,
                )

            self.logger.info(ui.strings.server_log_stopped)
            self.set_status('offline')
