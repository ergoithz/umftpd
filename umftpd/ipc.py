"""Safe IPC-related utilities."""

import hashlib
import hmac
import multiprocessing
import pickle
import typing

import tblib.pickling_support

from gi.repository import GLib

tblib.pickling_support.install()

protocol = pickle.HIGHEST_PROTOCOL
hmac_digest = hashlib.blake2b
hmac_size = hmac_digest().digest_size

def digest(data: bytes) -> bytes:
    process = multiprocessing.current_process()
    return hmac.digest(process.authkey, data, hmac_digest)

def dumps(data: typing.Any) -> bytes:
    serialized = pickle.dumps(data, protocol)
    return b''.join((digest(serialized), serialized))

def loads(data: bytes) -> typing.Any:
    signature, serialized = data[:hmac_size], data[hmac_size:]
    if hmac.compare_digest(signature, digest(serialized)):
        return pickle.loads(serialized)
    return None

def variant_dumps(data: typing.Any) -> GLib.Variant:
    return GLib.Variant('ay', dumps(data))

def variant_loads(data: GLib.Variant) -> typing.Any:
    return loads(data.get_data_as_bytes().unref_to_data())
