"""User interface micro-toolkit.

It is 2023 and yet some weirdos are still trying to push everybody into
writing XML while the only alternatives are an unmaintained GUI designer and
C-like imperative boilerplate, just insane.

This module provides a bunch of managers and declarative wrappers around
GObject APIs so UI can be defined, organized, and nested, in a sensible way.

"""
import collections
import collections.abc
import dataclasses
import functools
import importlib.resources
import importlib.util
import json
import os
import pathlib
import types
import typing
import warnings
import weakref

import umftpd.uitk.mlogic as mlogic
import mstache

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Adw, Gdk, GLib, Gio, GObject, Gtk

# WORKAROUND: libadwaita issue #677
if os.environ.pop('GTK_THEME', None):
    warnings.warn(
        'GTK_THEME environment variable is not supported',
        stacklevel=1,
        )

THandler = collections.abc.Callable[..., bool | None]

V = typing.TypeVar('V')
C = typing.TypeVar('C', bound=collections.abc.Callable)
G = typing.TypeVar('G', bound=GObject.Object)
R = typing.TypeVar('R', bound=Gtk.Root | Gtk.NativeDialog | None)
H = typing.TypeVar('H', bound=THandler)

_roots: list[Gtk.Root] = []
_named_objects = weakref.WeakValueDictionary[str, GObject.Object]()
_object_names: dict[GObject.Object, str] = {}
_init_callbacks: list[tuple[
    collections.abc.Callable[..., typing.Any],
    collections.abc.Iterable,
    collections.abc.Mapping,
    ]] = []


class DuplicatedKeyError(KeyError):
    """Key error due duplicated key in unique-key mappings."""


class DuplicatedWidgetNameError(KeyError):
    """Widget error due duplicated name in widget mappings."""


class DuplicatedDomainError(KeyError):
    """Domain error due duplicated domain name."""


class RootCollisionError(ValueError):
    """Widget error due its root being different to widget mapping root."""


@dataclasses.dataclass(order=True, frozen=True)
class Translation:
    code: str = 'C'
    translators: collections.abc.Sequence[str] = ()
    data: collections.abc.Mapping[str, str] = dataclasses.field(default_factory=dict)


class AttrMappingProxy(collections.abc.Mapping[str, V]):
    """AttrDict exposing all named objects.."""

    def __init__(self, mapping: collections.abc.Mapping) -> None:
        super().__init__()
        self._data = mapping

    def __getitem__(self, key: str) -> V:
        return self._data[key]

    def __iter__(self) -> collections.abc.Iterator[str]:
        return iter(self._data)

    def __len__(self) -> int:
        return len(self._data)

    def __getattr__(self, key: str) -> V:
        try:
            return self[key]
        except KeyError as e:
            raise AttributeError(key) from e


class IconManager:
    """Importlib-based Gtk icon resource manager."""

    _theme: Gtk.IconTheme | None = None

    def _bind(self) -> None:
        self._theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default())

    def __init__(self, namespaces: collections.abc.Iterable[str] = ()) -> None:
        """Initialize."""
        add_init_callback(self._bind)
        for namespace in namespaces:
            self.add_namespace(namespace)

    def add_namespace(self, namespace: str) -> None:
        """
        Register module namespace on default Gtk.IconTheme.

        `Gtk` requires icon resources to be on a `hicolor/{size}/{category}/`
        directory structure inside the namespace.

        """
        if self._theme is None:
            add_init_callback(self.add_namespace, namespace)
            return

        # filesystem case, register namespace path
        spec = importlib.util.find_spec(namespace)
        search_locations = spec.submodule_search_locations if spec else ()
        if search_locations:
            for path in search_locations:
                self._theme.add_search_path(path)
            return

        # virtual, cache tree structure
        application = UiApplication.get_default()
        directory = application.get_application_id() or namespace
        cache = pathlib.Path(GLib.get_user_cache_dir(), directory, 'icons')
        root = importlib.resources.files(namespace)
        queue = collections.deque([(cache, root)])
        while queue:
            dest, directory = queue.popleft()
            dest.mkdir(parents=True, exist_ok=True)
            for traversable in directory.iterdir():
                name = traversable.name
                if traversable.is_file():
                    (dest / name).write_bytes(traversable.read_bytes())
                elif traversable.is_dir():
                    queue.append((dest / name, traversable))
        self._theme.add_search_path(str(cache))


class MessageRenderer:
    """Mstache renderer callable and message renderer accessor."""

    def __init__(self, manager: 'MessageManager') -> None:
        self._manager = manager

    def __call__(self, key: str, **kwargs) -> str:
        return mstache.render(
            self._manager[key],
            kwargs,
            resolver=self._manager.get,
            getter=mlogic.getter,
            lambda_render=mlogic.lambda_render,
            )

    def __getattr__(self, key: str) -> collections.abc.Callable[..., str]:
        if self._manager.get(key):
            return functools.partial(self, key)
        raise AttributeError(key)


class MessageManager(AttrMappingProxy[str]):
    """Makeshift translatable message manager."""

    _renderer_class = MessageRenderer
    _all = weakref.WeakValueDictionary[str, typing.Self]()
    _localized: collections.abc.Mapping[str, str] = types.MappingProxyType({})
    _template: collections.abc.Mapping[str, typing.Any] = types.MappingProxyType({
        'translators': [
            'Translator Name <optional@email.here>',
            ],
        })

    @functools.cached_property
    def _translations(self) -> tuple[Translation, ...]:
        codes = frozenset(self._locales)
        return tuple(
            translation
            for translation in self.all_translations()
            if translation.code in codes or not codes
            )

    @functools.cached_property
    def render(self) -> _renderer_class:
        return self._renderer_class(self)

    def __init__(
            self,
            domain: str = 'global',
            namespace: str | None = None,
            *args, **fields,
            ) -> None:
        super().__init__(dict(*args, **fields))

        if self._all.get(domain, self)._data != self._data:
            raise DuplicatedDomainError(domain)

        self._domain = domain
        self._namespace = (
            importlib.resources.files(namespace) if namespace else
            None
            )
        self._locales = list({
            locale: None
            for name in GLib.get_language_names()
            if (locale := name.removeprefix('.UTF-8')) != 'C'
            })

        translation = self._translation(self._locales[::-1])
        self._translators = translation.translators
        self._localized = translation.data
        self._all[domain] = self

    def _translation(self, locales: collections.abc.Iterable[str]) -> Translation:
        if not self._namespace or not locales:
            return Translation()

        code = 'C'
        merged_translators = set()
        merged_translation = {}
        for locale in locales:
            try:
                data = json.loads(
                    (self._namespace / f'{locale}.json').read_text(),
                    )
            except FileNotFoundError:
                continue

            translation = {
                key: value['string']
                for key, value in data.get(self._domain, {}).items()
                if not key.startswith('_')
                }
            if not translation:
                continue

            code = locale
            translators = data.get('translators', ())

            if frozenset(translation).issuperset(merged_translation):
                merged_translators.clear()

            merged_translators.update(translators)
            merged_translation.update(translation)

        return Translation(
            code=code,
            translators=sorted(merged_translators),
            data=merged_translation,
            )

    def __getitem__(self, key: str) -> str:
        res = self._localized.get(key) or self._data.get(key)
        if res is None:
            raise AttributeError(key)
        return res

    @classmethod
    def all_translations(cls) -> list[Translation]:
        return sorted(
            manager._translation([traversable.name[:-5]])
            for manager in cls._all.values() if manager._namespace
            for traversable in manager._namespace.iterdir() if traversable.name.endswith('.json')
            )

    @classmethod
    def all_translators(cls) -> list[str]:
        translators = collections.defaultdict(set)
        for translation in cls.all_translations():
            for translator in translation.translators:
                translators[translator].add(translation.code)
        return sorted(
            f'{translator} ({", ".join(sorted(codes))})'
            for translator, codes in translators.items()
            )

    @classmethod
    def render_template(cls, template: str) -> str:
        return mstache.render(template, cls._all, resolver=cls._all.get)

    @classmethod
    def _dump_all(cls) -> dict[str, dict[str, dict[str, str]]]:
        return dict(cls._template) | {
            domain: {
                key: {'string': value}
                for key, value in manager._data.items()
                if not key.startswith('_')
                }
            for domain, manager in cls._all.items()
            }


class UiApplication(Adw.Application):
    """Base application class with StartupManager initalization."""

    actions: typing.ClassVar[collections.abc.Iterable[Gio.Action]] = ()
    action_accels: typing.ClassVar[collections.abc.Mapping[
        str,
        collections.abc.Iterable[str],
        ] | None] = None

    def __init__(self, *args, **kwargs) -> None:
        """Initialize."""
        super().__init__(*args, **kwargs)

        for action in self.actions:
            self.add_action(action)

        for action, accels in (self.action_accels or {}).items():
            self.set_accels_for_action(action, list(accels))

        for fnc, ar, kw in _init_callbacks:
            fnc(*ar, **kw)


class UiDemoAplication(UiApplication):
    """Application showing every knwon top-level window."""

    def do_activate(self) -> None:
        """Show every known window."""
        for root in _roots:
            if isinstance(root, Gtk.NativeDialog):
                root.show()
            elif isinstance(root, Gtk.Window):
                root.set_application(self)
                root.add_css_class('devel')
                root.show()


def build(cls: type[G], *args, **kwargs) -> G:
    """
    Create instance of given GObject, handling extra options.

    Parameters will be applied as properties in the constructor, if possible,
    otherwise they will be set via matching methods (see notes below).

    Notes
    =====

    If positional arguments are provided, `cls.new` method will be used
    if available (and so, all keyword arguments applied via method calls),
    regular class instancing will be used otherwise (with GObject options
    passed as constructor parameters, and all other keyword arguments applied
    via method calls).

    Property handling
    -----------------

    This is how keyword arguments are applied via method calls:

    - ``{name}``:
      Call ``widget.[set_]{name}(value)``.
    - ``{name}_apply``:
      Call ``widget.[set_]{name}(value(widget))``.
      Other modifiers can be chained before ``_apply``.
    - ``{name}_ref``:
      Call ``widget.[set_]{name}(_named_objects[value])``.
      Other modifiers can be chained before ``_ref``.
    - ``{name}_ref_many``:
      Loop value calling ``widget.[set_]{name}(_named_objects[value[i]])``.
      Other modifiers can be chained before ``_ref)many``.
    - ``{name}_on_[any_]{property}``:
      Call ``widget.get_{property}().[set_]{name}(value)``,
      but only if widget.get_{property}() returns non-falsy value.
      The ``any_`` modifier (after ``_on_``) will ignore cases where
      `get_{property}()` returns falsy, instead of raising an exception.
      Other modifiers (this included) can be chained before ``_on_``.
    - ``{name}_many``:
      Loop value calling ``widget.[set_]{name}(value[i])``.
    - ``{name}_args``:
      Call ``widget.[set_]{name}(*value)``.
    - ``{name}_kwargs``:
      Call ``widget.[set_]{name}(**value)``.
    - ``{name}_many_args``:
      Iterate and call ``for item in value: widget.[set_]{name}(*item)``.
    - ``{name}_many_kwargs``:
      Iterate and call ``for item in value: widget.[set_]{name}(**item) ``.

    """
    self = getattr(cls, 'new', cls)(*args) if args else cls(**{
        key: kwargs.pop(key)
        for key in tuple(kwargs)
        if cls.find_property(key) and key not in _attr_skip
        })
    for key, value in kwargs.items():
        obj, prop, calls = _create_calls(self, key, value)
        if obj:
            override = _attr_overrides.get(prop)
            pre = (obj,) if override else ()
            # add -> obj.[set_]add(...)
            fnc = (
                # name -> _register_object + set_property
                # apply -> value(obj)
                override
                # add -> add
                or getattr(obj, prop, None)
                 # range -> set_range
                or getattr(obj, f'set_{prop}', None)
                # raise AttributeError for prop
                or getattr(obj, prop)
                )
            for ar, kw in calls:
                fnc(*pre, *ar, **kw)

    if name := object_name(self):
        _register_object(self, name)

    if isinstance(self, Gtk.Root):
        _roots.append(self)

    return self


def _set_application(
        obj: Gtk.Window,
        value: bool | Gtk.Application | bool | None,
        ) -> None:
    if not value:
        obj.set_application(None)
        return
    app = (
        value if isinstance(value, Gtk.Application) else
        Gtk.Application.get_default()
        )
    if app:
        obj.set_application(app)
    else:
        app_connect('startup', obj.set_application)


def _apply(obj: G, fnc: collections.abc.Callable[[G], V]) -> V:
    return fnc(obj)


def _set_name(obj: GObject.Object, name: typing.Any) -> None:
    _register_object(obj, name)
    if type(obj).find_property('name') is not None:
        obj.set_property('name', name)


def _register_object(obj: GObject.Object, name: str) -> None:
    if _named_objects.get(name) not in (None, obj):
        msg = f'name collision {name!r}'
        raise KeyError(msg)
    if (old := _object_names.get(obj)) not in (None, name):
        _named_objects.pop(old, None)
    _named_objects[name] = obj
    _object_names[obj] = name


def _create_calls(
        obj: G,
        prop: str,
        value: typing.Any,
        ) -> tuple[G | typing.Any, str, list[tuple[list, dict]]]:
    """Iterate method calls from create keyword argument."""
    if prop != (updated := prop.removesuffix('_apply')):
        # add_apply -> add(value(widget))
        prop, value = updated, value(obj)

    if prop != (updated := prop.removesuffix('_ref')):
        # add_ref -> add(_named_objects[value])
        prop, value = updated, _named_objects[value]

    if prop != (updated := prop.removesuffix('_ref_many')):
        # add_ref_many -> add(_named_objects[x]) for x in value
        prop, value = f'{updated}_many', [_named_objects[x] for x in value]

    while '_on_' in prop and not hasattr(obj, prop):
        # add_on_child -> get_child().add(...)
        prop, target = prop.rsplit('_on_', 1)
        target, optional = (
            (target[4:], True)
            if target.startswith('any_') else
            (target, False)
            )

        method = getattr(obj, f'get_{target}')
        if obj := method():
            continue

        if optional:
            return obj, prop, ()

        message = f'{method!r} returned {obj!r}'
        raise TypeError(message)

    return (
        # add_many_args -> add(*x) for x in value
        (obj, prop[:-10], [(ar, {}) for ar in value])
        if prop.endswith('_many_args') else

        # add_args -> add(*value)
        (obj, prop[:-5], [(value, {})])
        if prop.endswith('_args') else

        # add_many_kwargs -> add(**x) for x in value
        (obj, prop[:-12], [((), kw) for kw in value])
        if prop.endswith('_many_kwargs') else

        # add_kwargs -> add(**value)
        (obj, prop[:-7], [((), value)])
        if prop.endswith('_kwargs') else

        # add_many -> add(x) for x in value
        (obj, prop[:-5], [((v,), {}) for v in value])
        if prop.endswith('_many') else

        # add -> add(value)
        (obj, prop, [((value,), {})])
        )


def add_init_callback(fnc: collections.abc.Callable, *args, **kwargs) -> None:
    if UiApplication.get_default():
        fnc(*args, **kwargs)
        return
    _init_callbacks.append((fnc, args, kwargs))


def wrap(cls: type[G], *args, **defaults) -> typing.Callable[..., G]:
    """Wrap given GObject into a factory with handling extra options."""
    return functools.partial(build, cls, *args, **defaults)


def object_name(obj: GObject.Object) -> str | None:
    return obj.find_property('name') and obj.get_property('name')


@typing.overload
def connect(
        name: str | None,
        signal: str,
        handler: None = None,
        *,
        args: collections.abc.Iterable = (),
        action: str | None = None,
        ) -> collections.abc.Callable[[H], H]:
    ...

@typing.overload
def connect(
        name: str | None,
        signal: str,
        handler: H,
        *,
        args: collections.abc.Iterable = (),
        action: str | None = None,
        ) -> H:
    ...

def connect(
        name: str | None,
        signal: str,
        handler: THandler | None = None,
        *,
        args: collections.abc.Iterable = (),
        action: str | None = None,
        ) -> collections.abc.Callable:
    if handler is None:
        return functools.partial(connect, name, signal, args=args, action=action)

    app = UiApplication.get_default()
    if app is None:
        add_init_callback(connect, name, signal, handler, args=args, action=action)
        return handler

    if isinstance(app, UiDemoAplication):
        return handler

    obj = app if name is None else _named_objects[name]
    if action and (obj := obj.lookup_action(action)) is None:
        raise KeyError(action)

    unbound = handler is getattr(type(app), handler.__name__, None)
    handler = getattr(app, handler.__name__) if unbound else handler
    obj.connect(signal, handler, *args)
    return handler


def action(
        name: str | None,
        action: str,
        signal: str = 'activate',
        handler: H | None = None,
        *,
        args: collections.abc.Iterable = (),
        ) -> H | collections.abc.Callable[[H], H]:
    return connect(name, signal, handler, args=args, action=action)


_attr_skip = frozenset((
    'application',
    ))
_attr_overrides = {
    'application': _set_application,
    'apply': _apply,
    'name': _set_name,
    'set_application': _set_application,
    'set_name': _set_name,
    }

ref = AttrMappingProxy[GObject.Object](_named_objects)
app_connect = functools.partial(connect, None)
app_action = functools.partial(action, None)
