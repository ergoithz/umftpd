r"""Simple conditional block implementation for mstache.

Mstache lambda and getter functions implementing mustache conditional logic
blocks.

This is a fully supported non-standard extension for mustache, as it can be
implemented in the same way in other languages with mustache libraries with
a proper lambda implementation (unlike chevron).

GPLv3 licensed.

"""

import ast
import contextlib
import operator
import re
import typing

import mstache

if typing.TYPE_CHECKING:
    import collections.abc

D = typing.TypeVar('D')

suppress_value_error = contextlib.suppress(ValueError)
missing = object()
operators = {
    '<': operator.lt,
    '<=': operator.le,
    '==': operator.eq,
    '===': operator.is_,
    '!=': operator.ne,
    '!==': operator.is_not,
    '>=': operator.ge,
    '>': operator.gt,
    '|': operator.or_,
    '^': operator.xor,
    '&': operator.and_,
    '&&': lambda *a: all(a),
    '||': lambda *a: any(a),
    }
re_conditional = re.compile(
    r'^\s*({operand})\s*({operator})\s*({operand})\s*$'.format(
        operand=fr'[^\s{"".join({c for k in operators for c in k})}]+',
        operator='|'.join(map(re.escape, operators)),
        ),
    re.VERBOSE,
    )


def literal(value: str, default: D = None) -> D | typing.Any:
    with suppress_value_error:
        return ast.literal_eval(value)
    return default


def lambda_render(
        scope: typing.Any,
        scopes: 'collections.abc.Sequence',
        default_lambda_render: mstache.LambdaRenderFunctionConstructor = (
            mstache.default_lambda_render
            ),
        default_getter: mstache.PropertyGetter = mstache.default_getter,
        **kwargs: dict[str, typing.Any],
        ) -> typing.Callable[..., str]:

    def render(content: str, *, match: re.Match | None = None) -> str:
        if match:
            left, operator_name, right = match.groups()
            if not operators[operator_name](
                    default_getter(scope, scopes, left, literal(left)),
                    default_getter(scope, scopes, right, literal(right)),
                    ):
                return ''
        return default_render(content)

    default_render = default_lambda_render(scope, scopes=scopes, **kwargs)
    return render


def getter(
        scope: typing.Any,
        scopes: 'collections.abc.Sequence',
        key: str,
        default: D = None,
        *,
        default_getter: mstache.PropertyGetter = mstache.default_getter,
        **kwargs: dict[str, typing.Any],
        ) -> D | typing.Callable[[str, callable], str] | typing.Any:
    value = default_getter(scope, scopes, key, missing, **kwargs)
    if value is missing:
        if match := re_conditional.match(key):
            return lambda content, render: render(content, match=match)
        return default
    return value
