"""Network utilities."""

import abc
import asyncio
import contextlib
import ipaddress
import logging
import os
import socket

import ifaddr
import zeroconf
import zeroconf.asyncio

import umftpd.aio as aio
import umftpd.config as cfg

from gi.repository import Gio, GLib

MIN_PORT = 1024 if cfg.UID else 1
MAX_PORT = 65535
BACKLOG = 32

logger = logging.getLogger(__name__)
actions = tuple(
    Gio.SimpleAction(
        name=name,
        parameter_type=vartype and GLib.VariantType(vartype),
        enabled=True,
        )
    for name, vartype in (
        ('server:init', None),
        ('server:start', None),
        ('server:session', 's'),
        ('server:disconnect', None),
        ('server:upload', 's'),
        ('server:download', 's'),
        )
    )


class Server:

    @property
    def readonly(self) -> bool:
        return (
            self.config.readonly
            or not os.access(self.config.directory, os.W_OK)
            )

    @property
    def services(self) -> list[zeroconf.ServiceInfo]:
        summary = cfg.metadata['summary']
        return [
            zeroconf.ServiceInfo(
                f'_{protocol}._tcp.local.',
                f'{summary}._{protocol}._tcp.local.',
                port=self.config.port,
                server=f'{socket.gethostname()}.local.',
                )
            for protocol in self.config.protocols
            ]

    def __init_subclass__(cls) -> None:
        cls.run = cls._run

    def __init__(self, config: cfg.Config, timeout: float) -> None:
        self.config = config
        self.timeout = timeout

    @abc.abstractmethod
    async def setup(self) -> None:
        pass

    @abc.abstractmethod
    async def mainloop(self) -> None:
        pass

    @classmethod
    async def run(cls, config: cfg.Config, *args, **kwargs) -> None:
        server_class = (
            ssh.SFTPServer if config.secure == cfg.SECURE_MODE_SSH else
            ftp.FTPServer
            )
        return await server_class.run(config, *args, **kwargs)

    @classmethod
    async def _run(cls, *args, **kwargs) -> None:

        async def register_service(service: zeroconf.ServiceInfo) -> None:
            try:
                await zc.async_register_service(service)
            except zeroconf.NonUniqueNameException:
                logger.exception('Cannot register zeroconf service')

        self = cls(*args, **kwargs)
        aio.activate('server:init')
        await self.setup()
        aio.activate('server:start')
        async with zeroconf.asyncio.AsyncZeroconf() as zc:
            await asyncio.gather(
                self.mainloop(),
                *map(register_service, self.services),
                )


def check_port(port: int, ipv6: bool = False) -> bool:
    """Check if port is available."""
    family = socket.AF_INET6 if ipv6 else socket.AF_INET
    sock = socket.socket(family, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    with contextlib.closing(sock), contextlib.suppress(OSError):
        sock.bind(('', port))
        return True
    return False


def route_host(loopback: bool = False) -> list[str]:
    """Get network addresses to current host."""
    return list(dict.fromkeys(
        str(address)
        for adapter in ifaddr.get_adapters()
        for ip in adapter.ips
        if (address := (
            ipaddress.IPv4Address(ip.ip) if ip.is_IPv4 else
            ipaddress.IPv6Address(ip.ip[0]) if ip.is_IPv6 else
            None))
           and (loopback or not address.is_loopback)
        ))


# circular imports
import umftpd.server.ftp as ftp
import umftpd.server.ssh as ssh
