"""Config classes."""

import collections.abc
import dataclasses
import datetime
import functools
import getpass
import importlib.metadata
import json
import logging
import os
import pathlib
import sys
import typing

import umftpd.crypto as crypto

from gi.repository import GLib

ConfigData = collections.abc.Mapping[str, typing.Any]

K = typing.TypeVar('K', bound='ConfigBase')
A = typing.TypeVar('A')
R = typing.TypeVar('R')

VERBOSE = '--verbose' in sys.argv
logging.basicConfig(level=(logging.WARNING, logging.DEBUG)[VERBOSE])

logger = logging.getLogger(__name__)

UID = os.getuid()

UTC = datetime.UTC
TZ = datetime.datetime.now().astimezone().tzinfo

FLATPAK = pathlib.Path('/.flatpak-info').exists()
FILESYSTEM_ENCODING = sys.getfilesystemencoding()
CWD = pathlib.Path.cwd()
USER_HOME = pathlib.Path.home()

APPCONFIG_DIR = pathlib.Path(
    GLib.get_user_config_dir() or USER_HOME / '.config',
    'umftpd',
    )
APPCONFIG_FILE = APPCONFIG_DIR / 'config.json'

DEFAULT_DIRECTORY = pathlib.Path(
    GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE)
    or USER_HOME / 'Public',
    )

SECURE_MODE_PLAIN = 0
SECURE_MODE_SECURE = 1
SECURE_MODE_SSH = 2

metadata = importlib.metadata.metadata(__package__ or 'umftpd')

urls = dict(
    item.split(', ', 1)
    for item in metadata.get_all('Project-URL', ())
    if ', ' in item
    )


class AuthConfig:
    __slots__ = ('config',)

    def __init__(self, config: 'Config') -> None:
        self.config = config

    @property
    def username(self) -> str | None:
        return self.config.username or self.config.default.username

    @property
    def home(self) -> str:
        return str(self.config.directory)

    def __call__(self, username: str, password: str) -> bool:
        if username != self.username:
            return False
        if self.config.password:
            return self.config.password == password
        if self.config.password_hash:
            return self.config.password_hash.validate(password)
        return self.config.default.auth(username, password)


class ConfigBase:

    def __post_init__(self, _default: bool = False) -> None:
        if _default:
            return

        for field, value in self._values():
            parser = getattr(self, f'_parse_{field.name}', None)
            try:
                if callable(parser):
                    self.__dict__[field.name] = parser(value)
            except (TypeError, ValueError, KeyError, IndexError):
                self.__dict__[field.name] = field.default

    def __eq__(self, other: typing.Any) -> bool:
        return isinstance(other, ConfigBase) and self.data == other.data

    @classmethod
    @functools.cache
    def _fields(cls) -> tuple[dataclasses.Field, ...]:
        return dataclasses.fields(cls)

    def _values(self) -> collections.abc.Generator[
            tuple[dataclasses.Field, typing.Any], None, None]:
        for field in self._fields():
            if not field.name.startswith('_'):
                value = getattr(self, field.name)
                default = (
                    fnc() if callable(fnc := field.default_factory) else
                    field.default
                    )
                if value != default:
                    yield field, value

    @property
    def data(self) -> dict[str, typing.Any]:
        return {
            field.name: value.data if isinstance(value, ConfigBase) else value
            for field, value in self._values()
            }

    @classmethod
    def from_data(cls, data: ConfigData) -> typing.Self:
        return cls(**{
            field.name: value
            for field, value in (
                (field, data.get(field.name, field.default))
                for field in cls._fields()
                if not field.name.startswith('_')
                )
            if field.default != value
            })


@dataclasses.dataclass(frozen=True, eq=False)
class Info(ConfigBase):

    last: datetime.datetime | None = None

    @property
    def data(self) -> dict[str, typing.Any]:
        data = dict(super().data)

        last = data.get('last')
        if isinstance(last, datetime.datetime):
            data['last'] = {
                'iso': last.isoformat(),
                'unix': last.astimezone(tz=UTC).timestamp(),
                }

        return data

    def _parse_last(self,
                    value: datetime.datetime
                           | float
                           | collections.abc.Mapping[
                                typing.Literal['unix'],
                                float,
                                ]
                           | None,
                    ) -> datetime.datetime | None:
        if isinstance(value, collections.abc.Mapping):
            value = value.get('unix')
        return (
            value if isinstance(value, datetime.datetime | None) else
            datetime.datetime.fromtimestamp(value, tz=UTC)
            )


@dataclasses.dataclass(frozen=True, eq=False)
class Ui(ConfigBase):

    logs: bool = False


@dataclasses.dataclass(frozen=True, eq=False)
class Config(ConfigBase):

    username: str | None = None
    password: str | None = None
    password_hash: 'crypto.CryptoHash | None' = None
    port: int = 2121 if UID else 21
    secure: int = 1
    directory: pathlib.Path = DEFAULT_DIRECTORY
    readonly: bool = True
    keyfile: pathlib.Path = APPCONFIG_DIR / 'key.pem'
    certfile: pathlib.Path = APPCONFIG_DIR / 'cert.pem'
    ui: Ui = dataclasses.field(default_factory=Ui)
    info: Info = dataclasses.field(default_factory=Info)

    _default: dataclasses.InitVar[bool] = False

    def __post_init__(self, _default: bool = False) -> None:
        super().__post_init__(_default)

        password, passhash = self.password, self.password_hash
        if password and not (passhash and passhash.validate(password)):
            if passhash:
                logger.warning('Ignoring password hash: missmatch')
            self.__dict__.update(
                password_hash=crypto.CryptoHash.from_text(password),
                )

    def _parse_username(self, value: str) -> str | None:
        if value != self.default.username:
            return value
        return None

    def _parse_password(self, value: str) -> str | None:
        if value != self.default.password:
            return value
        return None

    def _parse_password_hash(self,
                             value: ConfigData | crypto.CryptoHash | None,
                             ) -> crypto.CryptoHash | None:
        return (
            value if isinstance(value, crypto.CryptoHash) else
            crypto.CryptoHash.from_data(value) if value else
            None
            )

    def _parse_directory(self, value: os.PathLike) -> pathlib.Path:
        directory = pathlib.Path(value)
        return directory if directory.is_dir() else DEFAULT_DIRECTORY

    def _parse_secure(self, value: int) -> int:
        return int(value)

    def _parse_ui(self, value: Ui | ConfigData) -> Ui:
        return value if isinstance(value, Ui) else Ui.from_data(value)

    def _parse_info(self, value: Info | ConfigData) -> Info:
        return value if isinstance(value, Info) else Info.from_data(value)

    @property
    def default(self) -> typing.Self:
        return self.from_defaults()

    @property
    def transport(self) -> str | None:
        return (None, 'SSL/TLS', 'SSH')[self.secure]

    @property
    def protocols(self) -> tuple[str, ...]:
        return (('ftp',), ('ftp', 'ftps'), ('sftp', 'scp'))[self.secure]

    @property
    def ssl(self) -> crypto.SSL:
        return crypto.SSL.from_paths(
            self.keyfile,
            self.certfile,
            self.keyfile.is_relative_to(APPCONFIG_DIR),
            self.certfile.is_relative_to(APPCONFIG_DIR),
            )

    @property
    def auth(self) -> AuthConfig:
        return AuthConfig(self)

    @property
    def data(self) -> dict[str, typing.Any]:
        data = dict(super().data)

        if password_hash := data.get('password_hash'):
            data['password_hash'] = password_hash.data
            data.pop('password', None)

        if directory := data.get('directory'):
            data['directory'] = str(directory)

        return data

    def save(self) -> None:
        APPCONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
        APPCONFIG_FILE.write_text(json.dumps(self.data, indent=2))

    def with_updates(self, *args, **kwargs) -> 'Config':
        fixed = {f.name: v for f, v in zip(self._fields(), args, strict=False)}
        fixed.setdefault('_default', False)

        if any('password' in d for d in (fixed, kwargs)):
            fixed.setdefault('password_hash', None)

        current = self.data
        updates = {**fixed, **kwargs}
        updates.update(
            (k, {**current[k], **updates[k]})
            for k in frozenset(current).intersection(updates)
            if (isinstance(current[k], collections.abc.Mapping)
                and isinstance(updates[k], collections.abc.Mapping))
            )

        return self.from_data({**current, **updates})

    @classmethod
    @functools.cache
    def from_defaults(cls) -> typing.Self:
        return cls(
            username=getpass.getuser(),
            password=crypto.generate_human_password(),
            _default=True,
            )

    @classmethod
    def from_environment(cls) -> 'Config':
        try:
            return cls.from_data(json.loads(APPCONFIG_FILE.read_text()))
        except FileNotFoundError:
            pass
        except (ValueError, TypeError):
            logger.warning('Ignoring config file: invalid', exc_info=True)
        return cls()
