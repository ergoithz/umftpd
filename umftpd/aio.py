"""Asyncio utilities."""

import asyncio
import collections.abc
import concurrent.futures
import functools
import itertools
import logging
import multiprocessing
import multiprocessing.connection
import multiprocessing.process
import threading
import time
import types
import typing
import weakref

import umftpd.ipc as ipc

from gi.repository import Gio, GLib

logger = logging.getLogger(__name__)
logger_names = 'umftpd.server', 'pyftpdlib', 'asyncssh'

actions = (
    Gio.SimpleAction(
        name='aio:log',
        parameter_type=GLib.VariantType('ay'),
        enabled=True,
        ),
    )

T = typing.TypeVar('T')
M = typing.TypeVar('M', bound=types.ModuleType)
Future = concurrent.futures.Future
CallArgs = collections.abc.Iterable
CallKwargs = collections.abc.Mapping[str, typing.Any]

DEFAULT_WORKERS = 8


def resolved(value: T = None) -> Future[T]:
    future = Future()
    future.set_result(value)
    return future


def activate(
        name: str,
        code: str | None = None,
        value: typing.Any = None,
        ) -> None:
    if current := AIOServer.current:
        current.loop.create_task(
            current.send(None, None, None, (name, code, value)),
            name='activate',
            )
    elif threading.current_thread() is not threading.main_thread():
        GLib.idle_add(activate, name, code, value)
    elif app := Gio.Application.get_default():
        app.activate_action(name, GLib.Variant(code, value) if code else None)
    else:
        message = 'no action handler available'
        raise RuntimeError(message)


class ActionLogHandler(logging.Handler):
    def emit(self, record: logging.LogRecord) -> None:
        activate('aio:log', 'ay', ipc.dumps(record))

    @classmethod
    def setup(cls, names: collections.abc.Iterable[str]) -> 'ActionLogHandler':
        self = cls()
        for name in names:
            logger = logging.getLogger(name)
            logger.addHandler(self)
            logger.setLevel(logging.INFO)
        return self


class AIOServer:

    loop: asyncio.AbstractEventLoop
    current: 'AIOServer | None' = None
    action_log_handler_class: typing.ClassVar = ActionLogHandler

    def __init__(
            self,
            inbound: multiprocessing.connection.Connection,
            outbound: multiprocessing.connection.Connection,
            max_workers: int,
            ) -> None:
        self._running = True
        self._tasks = weakref.WeakValueDictionary()
        self._inbound = inbound
        self._outbound = outbound
        self._lock = asyncio.Lock()
        self.loop = asyncio.new_event_loop()
        self.loop.set_default_executor(concurrent.futures.ThreadPoolExecutor(max_workers))

    async def send(
            self,
            fid: int | None = None,
            cancel: bool | None = None,
            exc: Exception | None = None,
            result: typing.Any = None,
            ) -> None:
        async with self._lock:
            await asyncio.to_thread(self._outbound.send, (fid, cancel, exc, result))

    async def send_result(
            self,
            fid: int,
            coro: collections.abc.Awaitable,
            ) -> None:
        try:
            await self.send(fid, None, None, await coro)
        except asyncio.CancelledError:
            await self.send(fid, True)
        except Exception as e:
            await self.send(fid, None, e)

    async def handle_call(
            self,
            fid: int,
            fnc: collections.abc.Callable[..., typing.Any],
            *args,
            **kwargs,
            ) -> None:
        self._tasks[fid] = asyncio.create_task(
            self.send_result(fid, asyncio.to_thread(fnc, *args, **kwargs)),
            name=f'call-{fid}',
            )

    async def handle_submit(
            self,
            fid: int,
            fnc: collections.abc.Callable[..., collections.abc.Awaitable],
            *args,
            **kwargs,
            ) -> None:
        self._tasks[fid] = asyncio.create_task(
            self.send_result(fid, fnc(*args, **kwargs)),
            name=f'submit-{fid}',
            )

    async def handle_cancel(self, fid: int, tid: int) -> None:
        res = task.cancel() if (task := self._tasks.get(tid)) else False
        await self.send(fid, None, None, res)

    async def handle_stop(self, fid: int) -> None:
        self._running = False
        await self.send(fid)

    async def main(self) -> None:
        recv = functools.partial(asyncio.to_thread, self._inbound.recv)
        try:
            while self._running:
                name, fid, args, kwargs = await recv()
                try:
                    await getattr(self, f'handle_{name}')(fid, *args, **kwargs)
                except Exception as exc:
                    await self.send(fid, None, exc)
            await self.send(None, True)
        except Exception as exc:
            await self.send(None, False, exc)

    @classmethod
    def run(cls, *args, **kwargs) -> None:
        self = cls(*args, **kwargs)
        cls.current = self
        cls.action_log_handler_class.setup(logger_names)
        with asyncio.Runner(loop_factory=lambda: self.loop) as runner:
            runner.run(self.main())


class AIOProcess(concurrent.futures.Future):

    _process: multiprocessing.process.BaseProcess
    _inbound: multiprocessing.connection.Connection
    _outbound: multiprocessing.connection.Connection

    aioserver_class: typing.ClassVar = AIOServer
    process_check_interval: typing.ClassVar = 1.

    def _run(self) -> None:
        interval = self.process_check_interval
        while self._process.is_alive():
            while self._outbound.poll(interval):
                try:
                    fid, cancel, exc, res = self._outbound.recv()
                except EOFError as exc:
                    GLib.idle_add(self.set_exception, exc)
                    return

                if fid is None:
                    if res:
                        activate(*res)
                        continue
                    if not self.done():
                        GLib.idle_add(*(
                            (self.set_exception, exc) if exc else
                            (self.cancel,)
                            ))
                    return

                if future := self._futures.pop(fid, None):
                    GLib.idle_add(*(
                        (future.cancel,) if cancel else
                        (future.set_exception, exc) if exc else
                        (future.set_result, res)
                        ))

        message = f'{self!r} terminated abruptly ({self._process.exitcode})'
        GLib.idle_add(self.set_exception, RuntimeError(message))

    def _cleanup(self, fut: Future) -> None:
        timeout_at = time.monotonic() + 1
        if self._thread.is_alive():
            self._send('stop')
            # FIXME: no idle_add during shutdown
            #        future.result(timeout_at - time.monotonic()) not working
            self._thread.join(timeout_at - time.monotonic())
        self._process.join(timeout_at - time.monotonic())

    def _bubble(self, fid: int, fut: Future) -> None:
        if fut.cancelled() and self._futures.pop(fid, None) is fut:
            self._send('cancel', fid)

    def _send(self, method: str, *args, **kwargs) -> Future:
        fid, future = next(self._counter), concurrent.futures.Future()
        self._futures[fid] = future
        self._inbound.send((method, fid, args, kwargs))
        future.add_done_callback(functools.partial(self._bubble, fid))
        return future

    def __init__(
            self,
            max_workers: int = DEFAULT_WORKERS,
            timeout: float = 1.,
            ) -> None:
        super().__init__()

        self._timeout = timeout
        self._counter = itertools.count()
        self._futures = weakref.WeakValueDictionary()

        inbound, self._inbound  = multiprocessing.Pipe()
        self._outbound, outbound = multiprocessing.Pipe()
        self._process = multiprocessing.get_context('spawn').Process(
            name=self.aioserver_class.__name__,
            target=self.aioserver_class.run,
            args=(inbound, outbound, max_workers),
            daemon=True,
            )
        self._thread = threading.Thread(
            name=f'{type(self).__name__}',
            target=self._run,
            daemon=True,
            )

        self.add_done_callback(self._cleanup)

    def start(self) -> None:
        self._process.start()
        self._thread.start()

    def call(
            self,
            fnc: collections.abc.Callable[..., T],
            *args,
            **kwargs,
            ) -> Future[T]:
        return self._send('call', fnc, *args, **kwargs)

    def submit(
            self,
            fnc: collections.abc.Callable[..., collections.abc.Awaitable[T]],
            *args,
            **kwargs,
            ) -> Future[T]:
        return self._send('submit', fnc, *args, **kwargs)
