# Usermode FTP Server

A safe FTP server to access your files from anywhere, no root required.

Powered by [pyftpdlib](https://github.com/giampaolo/pyftpdlib), [asyncssh](https://github.com/ronf/asyncssh) and [Gtk4](https://www.gtk.org/).

## Install

<a href='https://flathub.org/apps/details/eu.ithz.umftpd'>
<img width="240" alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/>
</a>

Or via command-line:

```sh
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install --user eu.ithz.umftpd
```

Or downloading a distributable from our [project releases](https://gitlab.com/ergoithz/umftpd/-/releases).

## Documentation

We believe in simplicity, so we work hard make things intuitively easy.

If we failed on make things straightforward enough, please check out our [User Guide](https://gitlab.com/ergoithz/umftpd/-/wikis/User-Guide).

You might also be interested in [our guide on how to connect to an FTP server](https://gitlab.com/ergoithz/umftpd/-/wikis/User-Guide/Connecting-to-an-FTP-server).

More information on in [our wiki](https://gitlab.com/ergoithz/umftpd/-/wikis/home).

## Screenshots

<div style="text-align:center">
<a href="https://gitlab.com/ergoithz/umftpd/-/blob/master/media/v0.3.4-main.png">
<img width="400" src="https://gitlab.com/ergoithz/umftpd/-/raw/master/media/v0.3.4-main.png" alt="Main window"></img>
</a>
<a href="https://gitlab.com/ergoithz/umftpd/-/blob/master/media/v0.3.4-status.png">
<img width="400" src="https://gitlab.com/ergoithz/umftpd/-/raw/master/media/v0.3.4-status.png" alt="Status window"></img>
</a>
</div>

More screenshots in [our Screenshots page](https://gitlab.com/ergoithz/umftpd/-/wikis/Screenshots).

## Development

Check out our [Developer Guide](https://gitlab.com/ergoithz/umftpd/-/wikis/Developer-Guide).

## License

GNU Public License version 3 (see [LICENSE](https://gitlab.com/ergoithz/umftpd/-/blob/master/LICENSE)).
