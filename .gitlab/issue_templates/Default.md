## Pre-checks

[] The issue isn't listed in the [Known issues wiki page](https://gitlab.com/ergoithz/umftpd/-/wikis/User-Guide/Known-issues)
[] There is no other report of this issue in the [project issue tracker](https://gitlab.com/ergoithz/umftpd/-/issues)

## Information

- Linux distribution:
```sh
cat /etc/os-release
```
- Flatpak runtime and application versions:
```sh
flatpak --version
flatpak info eu.ithz.umftpd
flatpak info org.gnome.Platform//44
```
- (If possible) Application standard output when the issue happens:
```sh
flatpak run eu.ithz.umftpd
```

## Description

1. Steps to reproduce the issue: How? When?
2. Unexpected behavior: What happened?
3. Expected behavior: What should happened instead?
